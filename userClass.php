<?php
	
	class userClass{
		//Login a user
		public function userLogin($email, $password){
			$db = getDbconn();
			$stmt = $db->prepare("SELECT * FROM users WHERE email=:email AND password=:password");
			$stmt->execute(['email' => $email, 'password' => $password]);
			$user = $stmt->fetch(PDO::FETCH_OBJ);
			$count = $stmt->rowCount();
			if($count == 1){
				$_SESSION['id'] = $user->id;
				return true;
			}else{
				return false;
			}
		}
		
		//Register a user
		public function userRegister($email, $password, $first_name, $last_name){
			$db = getDbconn();
			$stmt = $db->prepare("INSERT INTO users(email, password, first_name, last_name) VALUES(:email, :password, :first_name, :last_name)");
			$stmt->execute(['email' => $email, 'password' => $password, 'first_name' => $first_name, 'last_name' => $last_name]);
			$_SESSION['id'] = $db->lastInsertId();
			return true;
		}
		
		//Return user data
		public function userDetails($id){
			$db = getDbconn();
			$stmt = $db->prepare("SELECT * FROM users WHERE id = :id");
			$stmt->execute(['id' => $id]);
			$userData = $stmt->fetch(PDO::FETCH_OBJ);
			return $userData;
		}
		
		//Insert a post
		public function insertPosts($user_id, $user, $title, $body){
			$db = getDbconn();
			$stmt = $db->prepare("INSERT INTO posts(user_id, user, title, body) VALUES(:user_id, :user, :title, :body)");
			$stmt->execute(['user_id' => $user_id, 'user' => $user, 'title' => $title, 'body' => $body]);
			return true;
		}
		
		//Show users posts
		public function showPosts($id){
			$db = getDbconn();
			//$stmt = $db->query("SELECT * FROM posts WHERE id = $id");
			$stmt = $db->prepare("SELECT * FROM posts WHERE id = :id");
			$stmt->execute(['id' => $id]);
			$count = $stmt->rowCount();
			if($count > 0){
				while($posts = $stmt->fetch(PDO::FETCH_OBJ)){
					return $posts;
				}
			}else{
				echo 'There are no posts available for you.';
			}
		}
		
		public function shownPosts(){
			$db = getDbconn();
			//$stmt = $db->query("SELECT * FROM posts WHERE id = $id");
			$stmt = $db->prepare("SELECT * FROM posts");
			$stmt->execute();
			$count = $stmt->rowCount();
			if($count > 0){
				while($posts = $stmt->fetch(PDO::FETCH_OBJ)){
					return $posts;
				}
			}else{
				echo 'There are no posts available for you.';
			}
		}
		
		//Returns the number of posts in the post table.
		public function showPostsCount(){
			$db = getDbconn();
			//$stmt = $db->query("SELECT * FROM posts WHERE id = $id");
			$stmt = $db->prepare("SELECT * FROM posts");
			$stmt->execute();
			$count = $stmt->rowCount();
			while($posts = $stmt->fetch(PDO::FETCH_OBJ)){
				return $count;
			}
		}
		
		//View a user's post
		public function viewPost($id){
			$db = getDbconn();
			$stmt = $db->prepare("SELECT * FROM posts WHERE id=:id");
			$stmt->execute(['id' => $id]);
			$post = $stmt->fetch(PDO::FETCH_OBJ);
			return $post;
		}
		
		//Update users posts
		public function updatePosts($title, $body, $id){
			$db = getDbconn();
			$stmt = $db->prepare("UPDATE posts SET title = :title, body = :body WHERE id = :id");
			$stmt->execute(['title' => $title, 'body' => $body, 'id' => $id]);
			return true;
		}
		
		//Delete users posts
		public function deletePosts($user_id, $id){
			$db = getDbconn();
			$stmt = $db->prepare("DELETE FROM posts WHERE user_id = :user_id AND id = :id");
			$stmt->execute(['user_id' => $user_id, 'id' => $id]);
			return true;
		}
	}
?>