<?php
	require('config.php'); //Connects to the database
	require('session.php'); //Checks if there is an active seesion, redirects to login page if none
	$userDetails=$userClass->userDetails($session_id);
	$id = $userClass->showPosts($id)->id;
	$viewPost = $userClass->viewPost($id);

	if(isset($_POST['title'])&&isset($_POST['body'])){
		$title = $_POST['title'];
		$body = $_POST['body'];
		$user = $_POST['user'];
		if(!empty($title)&&!empty($body)){
			$userClass->insertPosts($session_id, $user, $title, $body);
			echo '<script> location.href="home.php"; </script>';
		}
	}
	
	if($userClass->viewPost($id)){
			echo $viewPost->user ."<br />";
			echo $viewPost->title ."<br />";
			echo $viewPost->body ."<br />";
			
			?>
			
			<br />
			<br />
			<a href="editpost.php" class="button"> Edit post </a>
			<br />
			
			<?php
			if(isset($_POST['delete'])){
				if($userClass->deletePosts($session_id, $viewPost->id)){
					echo '<script> location.href="home.php"; </script>';
				}else{
					echo 'Unable to delete post';
				}
			}
	}
?>

<html>
	<head>
		<title>
			Social media
		</title>
		<link rel="stylesheet" type="text/css" href="social.css" />
	</head>
	<body>
		
		<div id="">
			<form action="" method="POST">
				<input type="submit" name="delete" value="Delete Post" title ="Click to delete your post" />
				
			</form>
		</div>
	</body>
	
</html>