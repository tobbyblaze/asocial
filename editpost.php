<?php

	require('config.php'); //Connects to the database
	require('session.php'); //Checks if there is an active seesion, redirects to login page if none
	$userDetails=$userClass->userDetails($session_id);
	$id = $userClass->showPosts()->id;
	$viewPost = $userClass->viewPost($id);

	if(isset($_POST['update'])){
		if($viewPost->user_id == $session_id){
			if($userClass->updatePosts($viewPost->user, $viewPost->title, $viewPost->body, $viewPost->id)){
				echo '<script> location.href="home.php"; </script>';
			}else{
				echo 'Unable to update post';
			}
		}else{
			echo 'You are not authorised to update this post';
		}
	}
?>
<html>
	<head>
		<title>
			Social media
		</title>
		<link rel="stylesheet" type="text/css" href="social.css" />
	</head>
	<body>
		<h1>Welcome <?php echo $userDetails->first_name; ?></h1>

		<div id="home">
			<form action="home.php" method="POST">
				<h1> What's happening? </h1>
				<br />
				
				<input type="hidden" name="user" value="<?php echo $userDetails->first_name; ?>" />
				
				<label for="title"> Title </label>
				<input type="text" title ="Enter your post title" name="title" id="title" value="<?php echo $viewPost->title; ?>" />
				<br />
				<label for="body"> Body </label>
				<input type="text" title ="Enter your post body" name="body" id="body" value="<?php echo $viewPost->body; ?>" />
				<br />
				
				<input type="submit" name="update" value="Update Post" title ="Click to update your post" />
				
			</form>
		</div>
		
		<h3>
			<a href="logout.php" class="button" title ="Click to log out of your account">Log out</a>
		</h3>
	</body>
</html>