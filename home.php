<?php
	require('config.php'); //Connects to the database
	require('session.php'); //Checks if there is an active seesion, redirects to login page if none
	$userDetails=$userClass->userDetails($session_id);
	/*
	for($id = 1; $id < 4; $id++){
		$showPosts=$userClass->showPosts($id);
	}
	*/

	if(isset($_POST['title'])&&isset($_POST['body'])){
		$title = $_POST['title'];
		$body = $_POST['body'];
		$user = $_POST['user'];
		if(!empty($title)&&!empty($body)){
			$userClass->insertPosts($session_id, $user, $title, $body);
			echo '<script> location.href="home.php"; </script>';
		}
	}
	
		for($id = $userClass->showPostsCount(); $id > 0; $id--){
			echo $userClass->showPosts($id)->user ."<br />";
			echo $userClass->showPosts($id)->title ."<br />";
			echo $userClass->showPosts($id)->body ."<br />";
			
			if(isset($_POST['delete'])){
				if($userClass->deletePosts($session_id, $userClass->showPosts($id)->id)){
					echo '<script> location.href="home.php"; </script>';
				}else{
					echo 'Unable to delete post';
				}
			}
			
			?>
			<br />
			
			<a href="viewpost.php" class="button"> View post </a>
			<br />
			<br />
			<br />
			
			<?php
			
		}
		
?>

<html>
	<head>
		<title>
			This is the main page
		</title>
		<link rel="stylesheet" type="text/css" href="social.css" />
	</head>
	<body>
		<h1>Welcome <?php echo $userDetails->first_name; ?></h1>

		<div id="home">
			<form action="home.php" method="POST">
				<h1> What's happening? </h1>
				<br />
				
				<input type="hidden" name="user" value="<?php echo $userDetails->first_name; ?>" />
				
				<label for="title"> Title </label>
				<input type="text" title ="Enter your post title" name="title" id="title" />
				<br />
				<label for="body"> Body </label>
				<input type="text" title ="Enter your post body" name="body" id="body" />
				<br />
				
				<input type="submit" value="Post" title ="Click to upload your post" />
				
			</form>
		</div>
		
		<h3>
			<a href="logout.php" class="button" title ="Click to log out of your account">Log out</a>
		</h3>
	</body>
</html>