<?php
	session_start();
	function getDbconn(){
		$host = "localhost";
		$dbname = "social";
		$dbuser = "root";
		$dbpassword = "";
		
		try{
			//$dbconn = new PDO("mysql:host=".$host.";dbname=".$dbname, $dbuser, $dbpassword);
			$dbconn = new PDO("mysql:host=localhost;dbname=social", $dbuser, $dbpassword);
			$dbconn->exec("set names utf8");
			$dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $dbconn;
		}catch (PDOException $e) {
			echo 'Connection failed: ' . $e->getMessage();
		}
	}
?>