<?php
	include("config.php");
	include('userClass.php');
	$userClass = new userClass();
	
	if(isset($_POST['email'])&&isset($_POST['password'])&&isset($_POST['first_name'])&&isset($_POST['last_name'])){
		$email = $_POST['email'];
		$password = $_POST['password'];
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		if(!empty($email)&&!empty($password)&&!empty($first_name)&&!empty($last_name)){
			if($userClass->userRegister($email, $password, $first_name, $last_name)){
				header("Location: home.php");
			}else{
				echo 'Unable to register user';
			}
		}
	}
?>

<html>
	<head>
		<title>
			Register page
		</title>
		<link rel="stylesheet" type="text/css" href="social.css" />
	</head>
	<body>
		<div id="signup">
			<h3>Registration</h3>
			<form method="post" action="" name="signup">
				<label for="email">Email</label>
				<input type="text" name="email" id="email" autocomplete="off" />
				<label for="password">Password</label>
				<input type="password" name="password" id="password" autocomplete="off" />
				<label for="first_name">First Name</label>
				<input type="text" name="first_name" id="first_name" autocomplete="off" />
				<label for="last_name">Last Name</label>
				<input type="text" name="last_name" id="last_name" autocomplete="off"/>
				<div class="errorMsg"><?php // echo $errorMsgReg; ?></div>
				<input type="submit" class="button" value="Signup">
			</form>
		</div>
	</body>
</html>