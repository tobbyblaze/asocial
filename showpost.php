<?php
	require('config.php'); //Connects to the database
	require('session.php'); //Checks if there is an active seesion, redirects to login page if none
	$userDetails=$userClass->userDetails($session_id);

	if(isset($_POST['title'])&&isset($_POST['body'])){
		$title = $_POST['title'];
		$body = $_POST['body'];
		$user = $_POST['user'];
		if(!empty($title)&&!empty($body)){
			$userClass->insertPosts($session_id, $user, $title, $body);
			echo '<script> location.href="home.php"; </script>';
		}
	}
	
	if($userClass->viewPost($id)){
			echo $showPosts->user ."<br />";
			echo $showPosts->title ."<br />";
			echo $showPosts->body ."<br />";
			
			if(isset($_POST['update'])){
				if($session_id=$showPosts->user_id){
					if($userClass->updatePosts($showPosts->user, $showPosts->title, $showPosts->body, $session_id)){
						echo '<script> location.href="home.php"; </script>';
					}else{
						echo 'Unable to update post';
					}
				}else{
					echo 'You are not authorised to update this post';
				}
			}
			
			if(isset($_POST['delete'])){
				if($userClass->deletePosts($session_id, $showPosts->id)){
					echo '<script> location.href="home.php"; </script>';
				}else{
					echo 'Unable to delete post';
				}
			}
		
		?>
		
		<form action="home.php" name="update" method="POST">
			<input class="button" type="submit" value="Update Post" />
		</form>
		
		<form action="home.php" method="POST">
			<input name="delete" type="hidden" />
			<input class="button" type="submit" value="Delete Post" />
		</form>
		
		<?php
	}
?>