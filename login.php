<?php
	include("config.php");
	include('userClass.php');
	$userClass = new userClass();

	if(isset($_POST['email'])&&isset($_POST['password'])){
		$email = $_POST['email'];
		$password = $_POST['password'];
		if(!empty($email)&&!empty($password)){
			if($userClass->userLogin($email, $password)){
				header("Location: home.php");
			}else{
				echo 'Unable to log in';
			}
		}
	}
?>

<html>
	<head>
		<title>
			Login page
		</title>
		<link rel="stylesheet" type="text/css" href="social.css" />
	</head>
	<body>
		<div id="login">
		<h3>Login</h3>
		<form method="post" action="" name="login">
		<label for="email">Email</label>
		<input type="text" id="email" name="email" autocomplete="off" />
		<label for="password">Password</label>
		<input type="password" id="password" name="password" autocomplete="off"/>
		<div class="errorMsg"><?php // echo $errorMsgLogin; ?></div>
		<input type="submit" class="button" value="Login">
		</form>
		</div>
	</body>
</html>